Source: libvi-quickfix-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Ernesto Hernández-Novich (USB) <emhn@usb.ve>
Section: perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libvi-quickfix-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libvi-quickfix-perl.git
Homepage: https://metacpan.org/release/Vi-QuickFix
Testsuite: autopkgtest-pkg-perl

Package: libvi-quickfix-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Suggests: vim
Multi-Arch: foreign
Description: Perl support for vim's QuickFix mode
 If a Perl program or module uses Vi::QuickFix, Perl logs errors and
 warnings to an error file. While editing that program or module with vim
 you can ask Perl to compile it. If there are errors during compilation,
 the error file is picked when you type a special QuickFix command and
 Vim will jump to the location of the first error recorded. Other
 QuickFix commands allow you to jump to other error messages, switching
 files if necessary.
 .
 This module is intended as a debugging tool for Perl programmers using
 Vim.
